var imageApp = angular.module('starter', ['ionic', 'ngCordova', 'firebase']);
var fb = new Firebase("https://cityofsjapp.firebaseio.com");

imageApp.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

imageApp.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
      .state("firebase", {
        url: "/firebase",
        templateUrl: "templates/firebase.html",
        controller: "FirebaseController",
        cache: false
    })
      .state("camera", {
        url: "/camera",
        templateUrl: "index.html",
        controller: "CameraController",
    })
     .state("map", {
      url: "/map",
      templateUrl: "templates/map.html",
      controller: "MapController",
    });
  $urlRouterProvider.otherwise("/firebase");
});

imageApp.controller("FirebaseController", function($scope, $state, $firebaseAuth) {

  var fbAuth = $firebaseAuth(fb);

  $scope.login = function(username, password) {
    fbAuth.$authWithPassword({
      email: username,
      password: password
    }).then(function(authData) {
      $state.go("map");
    }).catch(function(error) {
      console.error("ERROR: " + error);
    });
  }
    
  $scope.register = function(username, password) {
    fbAuth.$createUser({email: username, password: password}).then(function(userData) {
      return fbAuth.$authWithPassword({
          email: username,
          password: password
        });
    }).then(function(authData) {
        $state.go("map");
    }).catch(function(error) {
        console.error("ERROR: " + error);
    });
  }

});

imageApp.controller('MapController', function($scope) {

  google.maps.event.addDomListener(window, "load", function() {
    var myLatlng = new google.maps.LatLng(37.300, -120.4833);

    var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    navigator.geolocation.getCurrentPosition(function(pos) {
              map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    });

    $scope.map = map;
  
  });

});

imageApp.controller("CameraController", function($scope, $ionicHistory, $firebaseArray, $cordovaCamera) {

  $ionicHistory.clearHistory();
  $scope.images = [];

  var fbAuth = fb.getAuth();
  if(fbAuth) {
    var userReferences = fb.child("users/" + fbAuth.uid);
    var syncArray = $firebaseArray(userReferences.child("images"));
    $scope.images = syncArray;
  } else {
    $state.go("firebase");
  }
  
  $scope.upload = function() {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourcetype: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: Camera.popoverOptions,
      targetWidth: 500,
      targetHeight: 500,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {
      syncArray.$add({image: imageData}).then(function() {
        alert("The Image Was Saved")
      });
    }, function(error) {
      console.error("ERROR:" + error);
    });
    }
  });